<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* @author : Dian
* @copyright : 2015
*/
class Processing extends MY_Controller
{
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$scripts = array();
		$scripts[] = 'jquery.validate.js';
		$scripts[] = 'validate.js';
		$scripts[] = 'genetic.js';
        $this->data['footer']['scripts'] = $scripts;
		$this->view();
	}

// fungsi ini untuk menghitung kalori kebutuhan pasien dalam satu hari

	public function count_weight()
	{
		$response = new stdClass();
		$response->status = true;
		$response->data = new stdClass();
		$response->data->user = $this->request;
		$response->data->bbi = $this->hitungBeratBadanIdeal($this->request->tinggi_badan, $this->request->jenis_kelamin);
		
		$edema = 0;
		if ($this->request->edema == 'ringan') {
			$edema = ($response->data->bbi * 0.05);
		}else if ($this->request->edema == 'sedang berat') {
			$edema = ($response->data->bbi * 0.1);
		}

		$kkal = 30;
		if ($this->request->jenis_kelamin == "perempuan") {
			$kkal = 25;
		}
		
		$response->data->edema = $edema;
		$response->data->total_kalori = ($response->data->bbi - $edema) * $kkal;

		$this->load->model('Jenisdiet_model');
		$response->data->jenis_diet = $this->Jenisdiet_model->getByKalori($response->data->total_kalori);

		$this->session->set_userdata('kalori', $response->data);

		$this->json($response);
	}

// Proses yang akan dilakukan setelah melakukan input data pasien 


	public function process()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('genetika');
		$this->load->model('Makanan_model');
		$jumlah_generasi_awal = $this->input->post('generasi_awal');
		$iterasi = $this->input->post('iterasi');
		$grafik = array();

		$kalori = $this->session->kalori;
		$genetika = new Genetika();

		try{
			$kalori->jenis_diet->kalori = $kalori->total_kalori;
			$genetika->setDiet($kalori->jenis_diet);
			$genetika->setAlergi($kalori->user->alergi);
			$genetika->bangkitkanIndividu($jumlah_generasi_awal);
			$debug['generate'] = $genetika->individu;
			$genetika->log_item->populasi = $genetika->getIndividu();
			for ($i=0; $i < $iterasi; $i++) { 
				$grafik[] = array($i+1, $genetika->individu[0]->fitness);
				$genetika->crossOver();
				$genetika->mutation();
				$genetika->sorting();

				$genetika->log_list[] = $genetika->log_item;
				$genetika->log_item = new stdClass();
				$genetika->log_item->populasi = $genetika->getIndividu();
			}
			
			$result = array(
				'status' => true,
				'data'	 => array(),
				'debug'  => null,
				'makanan'=> null,
				'grafik' => (object)array('populasi' => $jumlah_generasi_awal, 'data' =>$grafik),
				'user'	 => $kalori->user
			);
			for ($i=0; $i < 7; $i++) { 
				$menu = array();
				foreach ($genetika->individu[$i]->_gen as $gen) {
					$makanan = (array)$this->Makanan_model->getByCode($gen->_id);
					$makanan['kalori'] = $gen->_value;
					$makanan['weight'] = $gen->_weight;
					$menu[] = $makanan;
				}
				$result['data'][$i] = new stdClass();
				$result['data'][$i]->menu = $menu;
				$result['data'][$i]->total_kalori = $genetika->individu[$i]->total_kalori;
				$result['data'][$i]->kandungan_gizi = $genetika->individu[$i]->getKandunganGiziMakanan();
			}

			$result['debug'] = $genetika->debug;
			$result['makanan'] = $genetika->list_makanan;
			$result['individu'] = $genetika->individu;

			$this->session->set_userdata('menu', $result['data']);
			$this->session->set_userdata('grafik', $result['grafik']);

			file_put_contents(FCPATH . "assets/log.json", json_encode($genetika->log_list));
			$this->json($result);
		}catch(Exception $e)
		{
			$this->json($genetika->debug);
		}
	}

	public function show_ga(){
		$scripts = array();
		$scripts[] = 'jqplot/jquery.jqplot.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.json2.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.barRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.categoryAxisRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.pointLabels.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasTextRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js';
		$scripts[] = 'show_ga.js';
        $this->data['footer']['scripts'] = $scripts;

        $style[] = array(
            'assets' => TRUE,
            'href' => 'javascripts/jqplot/css/jquery.jqplot.min.css');
        $this->data['header']['style'] = $style;

        if ($this->isPost()) {
        	$grafik = $this->session->grafik;
        	$this->json($grafik);
        }

		$this->view();
	}
	
	//menyimpan output berupa menu makanan dan data pasien ke dalam database
	public function save()
	{
		//print_r($this->session->kalori);
		$this->load->model('Menu_model');
		$this->load->model('Pasien_model');
		$user = (array)$this->session->kalori->user;

		$id_pasien = $this->Pasien_model->insert($user);
		$menu = $this->session->menu;
		for ($i=1; $i <= count($menu); $i++) { 
			foreach ($menu[$i - 1]->menu as $item) {
				$data = array(
					'id_pasien' => $id_pasien,
					'code'		=> $item['code'],
					'kalori'	=> $item['kalori'],
					'weight'	=> $item['weight'],
					'hari'		=> $i
				);
				$this->Menu_model->insert($data);
			}
		}

		redirect('processing/history');
	}

//menampilkan data pasien beserta menu makanannya ke dalam history
	public function history()
	{
		$scripts = array();
		$scripts[] = 'history.js';
		$this->data['footer']['scripts'] = $scripts;

		$this->load->model('Pasien_model');
		$listPasien = $this->Pasien_model->getAll();
		$this->data['view']['listPasien'] = $listPasien;
		$this->view();
	}

	public function detail($id)
	{
		$this->load->model('Pasien_model');
		$this->load->model('Menu_model');

		$this->data['view']['pasien'] = $this->Pasien_model->getByPasien($id);

		$menu = array();
		for ($i=1; $i <= 7; $i++) { 
			$menu_sehari = array();
			$data = $this->Menu_model->getByPasien($id, $i);
			foreach ($data as $item) {
				$menu_sehari[] = $item;
			}
			$menu[] = $menu_sehari;
		}
		$this->data['view']['menu'] = $menu;
		$this->view();
	}

//menghapus history yang sudah disimpan
	
	public function delete($id)
	{
		$this->load->model('Pasien_model');
		$this->Pasien_model->delete($id);

		$this->json(array('status' => true));
	}

	public function show_log($id)
	{
		ini_set('memory_limit', '-1');
		if (file_exists(FCPATH . "assets/log.json")) {
			$content = file_get_contents(FCPATH . "assets/log.json");

			$scripts = array();
			$scripts[] = 'paging.js';
			$this->data['footer']['scripts'] = $scripts;

			$logs = json_decode($content);
			$log = $logs[$id-1];
			$this->data['view']['current'] = $id;
			$this->data['view']['total'] = count($logs);
			$this->data['view']['log'] = $log;
			$this->view();
		}
	}

	/* private function */
	private function hitungBeratBadanIdeal($tinggi_badan, $jenis_kelamin)
	{
		if ($jenis_kelamin == "laki-laki") {
			if ($tinggi_badan <= 160) {
				return $tinggi_badan - 100;
			} else {
				return ($tinggi_badan - 100) * 0.9;
			}
		}else{
			if ($tinggi_badan <= 150) {
				return $tinggi_badan - 100;
			}else{
				return ($tinggi_badan - 100) * 0.9;
			}
		}
	}
}
?>