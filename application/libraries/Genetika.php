<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('GENETIKA_ROOT')) {
    define('GENETIKA_ROOT', dirname(__FILE__) . '/');
    require(GENETIKA_ROOT . 'Genetika/Kromosom.php');
    require(GENETIKA_ROOT . 'Genetika/Gen.php');
}

class Genetika
{

	/**
	 * Instance of this class
	 *
	 * @access	private
	 * @var Genetika
	 */
	private static $_instance;

	/**
	* Diet object
	*
	* @var stdClass
	*/	
	private $_jenis_diet;

	/**
	* Individu object
	*
	* @var array
	*/
	public $individu;

	/**
	* CI Object
	*
	* @var Object
	*/
	private $CI;

	/**
	* Bahan makanan object
	*
	* @var Object
	*/
	public $list_makanan;

	/**
	* object yang digunakan untuk menyimpan nilai random bahan makanan yang muncul
	*
	* @var Object
	*/
	private $list_random;

	/**
	* peluang mutasi. Nilai yang digunakan untuk menghitung berapa banyak gen yang akan di mutasi
	*
	* @var float
	*/
	public $pm;

	/**
	* peluang cross over. Nilai yang digunakan untuk menghitung berapa banyak kromosom yang akan di crossover
	*
	* @var float
	*/
	public $cm;

	/**
	* array untuk menyimpan bahan makanan yang menjadi alergi
	*
	* @var array
	*/
	private $alergi;

	/**
	* array untuk menyimpan bahan makanan yang diinginkan
	*
	* @var array
	*/
	private $makanan_diinginkan;

	public $log_list;
	public $log_item;

//mendeklarasikan konstanta

	public $debug;

	const MAKANAN_POKOK = 'p';
	const LAUK_PAUK = 'l';
	const SAYUR = 's';
	const BUAH = 'b';
	const PELENGKAP = 'pl';

	public function __construct()
	{
		$this->individu = array();
		$this->CI =& get_instance();
		$this->CI->load->model('Makanan_model');

		$this->list_makanan = new stdClass();
		$this->list_makanan->{self::MAKANAN_POKOK} = null;
		$this->list_makanan->{self::LAUK_PAUK} = null;
		$this->list_makanan->{self::SAYUR} = null;
		$this->list_makanan->{self::BUAH} = null;
		$this->list_makanan->{self::PELENGKAP} = null;

		$this->list_random = new stdClass();
		$this->list_random->{self::MAKANAN_POKOK} = array();
		$this->list_random->{self::LAUK_PAUK} = array();
		$this->list_random->{self::SAYUR} = array();
		$this->list_random->{self::BUAH} = array();
		$this->list_random->{self::PELENGKAP} = array();

		$this->pm = 0.2;
		$this->cm = 0.1;

		$debug = array();

		$this->log_list = array();
		$this->log_item = new stdClass();
	}

	/*
	* set diet user
	*/
	public function setDiet($diet)
	{
		$this->_jenis_diet = $diet;
	}

	/*
	* set bahan makanan yang menjadi alergi
	*/
	public function setAlergi($alergi)
	{
		$this->alergi = array();
		if(isset($alergi))
		{
			$this->alergi = explode(",",$alergi);
		}
	}

	/*
	* set bahan makanan yang diiginkan oleh user
	*/
	public function setMakananYgDiinginkan($list_makanan)
	{
		$this->makanan_diinginkan = array();
		$this->makanan_diinginkan = explode(",", $list_makanan);
	}

	/*
	* generate individul awal
	* individu merupakan object bentukan dari class Kromosom
	* jumlah gen adalah 15 gen terdiri dari 3 kromosom (pagi, siang, malam) * 5 gen/kromosom (jenis bahan makanan)
	*/
	public function bangkitkanIndividu($total_individu)
	{
		$total_kalori = $this->_jenis_diet->kalori / 3;

		for ($i=0; $i < $total_individu; $i++) { 
			$kromosom = new Genetika_Kromosom();
			$kromosom->kalori_diet = $this->_jenis_diet->kalori;
			for ($j=0; $j < 3; $j++) { 
				$makanan = $this->getMakanan(self::MAKANAN_POKOK, $total_kalori);
				$kromosom->_gen[0 + ($j * 5)] = $makanan;

				$makanan = $this->getMakanan(self::LAUK_PAUK, $total_kalori);
				$kromosom->_gen[1 + ($j * 5)] = $makanan;
				
				$makanan = $this->getMakanan(self::SAYUR, $total_kalori);
				$kromosom->_gen[2 + ($j * 5)] = $makanan;

				$makanan = $this->getMakanan(self::BUAH, $total_kalori);
				$kromosom->_gen[3 + ($j * 5)] = $makanan;

				$makanan = $this->getMakanan(self::PELENGKAP, $total_kalori);
				$kromosom->_gen[4 + ($j * 5)] = $makanan;
			}

			$kromosom->hitungFitness();
			$this->individu[] = $kromosom;
		}
		$this->sorting();
	}

	/*
	* melakukan cross over terhadap 2 individu terbaik (fitness terkecil)
	* posisi gen yang akan di cross over dirandom
	* hasil akhir akan tercipta 2 generasi baru
	*/
	public function crossOver()
	{
		$jmlh_individu = (int)($this->cm * count($this->individu));
		$jmlh_individu = ($jmlh_individu % 2 == 0) ? $jmlh_individu : $jmlh_individu+1;

		$this->log_item->crossover = new stdClass();
		$this->log_item->crossover->jumlah_individu = $jmlh_individu;
		$this->log_item->crossover->process = array();

		$index = 0;
		while ($index < $jmlh_individu) {
			$process = new stdClass();

			$individu = new Genetika_Kromosom(); 
			$individu->kalori_diet = $this->individu[$index]->kalori_diet;

			$index_awal = rand(1, count($this->individu[$index]->_gen)-2);

			$process->index_awal = $index_awal;

			for ($i=0; $i < count($this->individu[$index]->_gen); $i++) { 
				if ($i < $index_awal) {
					$individu->_gen[$i] = $this->individu[$index]->_gen[$i];
				}else{
					$individu->_gen[$i] = $this->individu[$index + 1]->_gen[$i];
				}
			}

			$individu->hitungFitness();
			$this->individu[] = $individu;

			$process->parent1 = $this->individu[$index]->getGenValue();
			$process->parent2 = $this->individu[$index + 1]->getGenValue();
			$process->child = $individu->getGenValue();
			$this->log_item->crossover->process[] = $process;

			$index += 2;
			// echo "$jmlh_individu - $index \n ";
		}
	}

	/*
	* proses mutasi gen
	* posisi gen yang akan di mutasi akan di random
	*/
	public function mutation()
	{
		$this->log_item->mutation = new stdClass();

		$total_mutation_gen = $this->pm * count($this->individu) * 15;

		$this->log_item->mutation->jumlah_gen = $total_mutation_gen;
		$this->log_item->mutation->process = array();

		for ($i=0; $i < $total_mutation_gen; $i++) { 
			$process = new stdClass();

			$randGen = rand(0, 14);
			$randKrom = rand(0, count($this->individu) - 1);
			$swapIndexGen = $randGen + 5;

			if ($randGen >= 10) {
				$swapIndexGen = $randGen % 5;
			}

			$process->posisi_gen = $randGen;
			$process->posisi_individu = $randKrom;
			$process->individu_awal = $this->individu[$randKrom]->getGenValue();

			$genTemp = $this->individu[$randKrom]->_gen[$randGen];
			$this->individu[$randKrom]->_gen[$randGen] = $this->individu[$randKrom]->_gen[$swapIndexGen];
			$this->individu[$randKrom]->_gen[$swapIndexGen] = $genTemp;

			$process->individu_akhir = $this->individu[$randKrom]->getGenValue();

			$this->log_item->mutation->process[] = $process;
		}
	}

	/* private function */
	private function getMakanan($type, $total_kalori)
	{
		$makanan = null;
		$tempKalori = 0;
		if ($type == self::MAKANAN_POKOK) {
			$tempKalori = $total_kalori * 0.45;
		}elseif ($type == self::LAUK_PAUK) {
			$tempKalori = $total_kalori * 0.20;
		}elseif ($type == self::SAYUR) {
			$tempKalori = $total_kalori * 0.15;
		}elseif ($type == self::BUAH) {
			$tempKalori = $total_kalori * 0.15;
		}elseif ($type == self::PELENGKAP) {
			$tempKalori = $total_kalori * 0.20;
		}

		$this->debug[$type][] = "get makanan. type => $type";
		$this->setMakanan($type, $tempKalori);
		
		/* untuk menentukan index random untuk bahan makanan, jika bahan makanan sudah pernah diambil
		 * random dilakukan lagi.
		 * random yang dilakukan adalah random tidak berulang. 
		 * Nilai random boleh berulang jikaa semua bahan 
		 * makanan sudah pernah di ambil 
		*/
		$rand = rand(0, count($this->list_makanan->{$type}) - 1);
		if (count($this->list_makanan->{$type}) == count($this->list_random->{$type})) {
			$this->debug[$type][] = "reset";
			$this->list_random->{$type} = array();
		}

		while (in_array($rand, $this->list_random->{$type})) {
			$rand = rand(0, count($this->list_makanan->{$type}) - 1);
		}

		$this->debug[$type][] = "index = $rand";

		// menyimpan bilangan unik hasil random
		$this->list_random->{$type}[] = $rand;
		/* end */

		$makanan = $this->list_makanan->{$type}[$rand];
		$gen = new Genetika_Gen($makanan->id, $makanan->value, $makanan->weight);
		$gen->setGen($makanan);

		return $gen;
	}

	/**
	 * untuk mengambil bahan makanan sesuai tipe dari database
	 * kemudian dihitung berat bahan makanan jika kalori makanan tersebut 
	 * melebihi dari kalori yang dibutuhkan.
	 * Hasil pengambilan akan disimpan dalam object $this->bahan_makanan->{type makanan}
	 *
	 * @var string $type
	 * @var int $tempKalori
	*/
	private function setMakanan($type, $tempKalori)
	{
		if (!isset($this->list_makanan->{$type})) {
			$this->debug[$type][] = "normalisasi. type => $type";
			$makanan = $this->CI->Makanan_model->getByType($type, 0, $this->alergi);

			$arrMakanan = array();
			foreach ($makanan as $item) {
				$data = new stdClass();
				$data->id = $item->code;
				if ($item->kj > $tempKalori) {
					$ratio = ($tempKalori / $item->kj);

					$data->value = $tempKalori;
					$data->weight = (int)($ratio * 100);
					$data->protein = ($ratio * $item->kd_protein);
					$data->lemak = ($ratio * $item->kd_fat);
					$data->carbohydrat = ($ratio * $item->kd_carbohydrat);
					$data->sukrosa = ($ratio * $item->kd_sucrose);
					$data->natrium = 0;
					$data->kolesterol = ($ratio * $item->kd_cholesterol);
					$data->serat = 0;
				}else{
					$data->value = $item->kj;
					$data->weight = 100;
					$data->protein = $item->kd_protein;
					$data->lemak = $item->kd_fat;
					$data->carbohydrat = $item->kd_carbohydrat;
					$data->sukrosa = $item->kd_sucrose;
					$data->natrium = 0;
					$data->kolesterol = $item->kd_cholesterol;
					$data->serat = 0;
				}
				$arrMakanan[] = $data;
			}

			$this->list_makanan->{$type} = $arrMakanan;
		}
	}

	/*
	* mengutukan / sorting individu berdasarkan fitness 
	* ASC
	*/
	public function sorting()
	{
		for ($i=0; $i < count($this->individu) - 1; $i++) { 
			$smallest = $this->individu[$i]->fitness;
			$index = $i;
			for ($j=$i+1; $j < count($this->individu) ; $j++) { 
				if ($this->individu[$j]->fitness < $smallest) {
					$index = $j;
					$smallest = $this->individu[$j]->fitness;
				}
			}
			
			//swap
			$temp = $this->individu[$i];
			$this->individu[$i] = $this->individu[$index];
			$this->individu[$index] = $temp;
		}
	}

	public function getIndividu()
	{
		$populasi = array();
		foreach ($this->individu as $key => $item) {
			$individu = new stdClass();
			$individu->fitness = $item->fitness;
			$individu->gen = $item->getGenValue();
			$populasi[] = $individu;
		}

		return $populasi;

	}
}
?>