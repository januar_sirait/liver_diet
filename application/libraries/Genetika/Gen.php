
<?php 

//membuat variabel yang diperlukan untuk membangun sebuah gen

class Genetika_Gen
{
	/**
	* Gen id
	* @var string
	*/
	public $_id;

	/**
	* gen value
	* @var int
	*/
	public $_value;

	/**
	* weight of food
	* @var int
	*/
	public $_weight;

	public $_protein;
	public $_lemak;
	public $_karbohidrat;
	public $_sukrosa;
	public $_natrium;
	public $_kolesterol;
	public $_serat;


	function __construct($id, $value, $weight)
	{
		$this->_id = $id;
		$this->_value = $value;
		$this->_weight = $weight;
	}

	function setGen($data)
	{
		$this->_protein = $data->protein;
		$this->_lemak = $data->lemak;
		$this->_karbohidrat = $data->carbohydrat;
		$this->_sukrosa = $data->sukrosa;
		$this->_natrium = $data->natrium;
		$this->_kolesterol = $data->kolesterol;
		$this->_serat = $data->serat;
	}

	/**
	* setter getter
	*/

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function getId()
	{
		return $this->_id;
	}

	public function setValue($value)
	{
		$this->_value = $value;
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function setWeight($weight)
	{
		$this->_weight = $weight;
	}

	public function getWeight()
	{
		return $this->_weight;
	}
}
?>