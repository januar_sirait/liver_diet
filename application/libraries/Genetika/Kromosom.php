<?php 

// membuat variabel yang diperlukan untuk membangun sebuah kromosom

class Genetika_Kromosom
{
	/**
	* Gen object
	* @var array
	*/
	public $_gen;

	/**
	* Fitness object
	*
	* @var float
	*/
	public $fitness;

	/**
	* kalori diet
	*
	* @var float
	*/
	public $kalori_diet;

	/**
	* total kalori_diet dari gen
	*
	* @var float
	*/
	public $total_kalori;

	function __construct()
	{
		$this->_gen = array();
	}

	public function getGen()
	{
		return $this->_gen;
	}

	public function getGenValue()
	{
		$gen = array();
		foreach ($this->_gen as $key => $item) {
			$gen[] = $item->_value;
		}

		return $gen;
	}

	public function setGet($gen)
	{
		$this->_gen = $gen;
	}

//fungsi untuk menghitung fitness 
// fitness = |kalori diet - total kalori|
	public function hitungFitness()
	{
		$total_kalori = 0;
		foreach ($this->_gen as $gen) {
			$total_kalori += $gen->getValue();
		}

		//menghitung bahan makanan yang memiliki kemiripan

		$this->fitness = abs($this->kalori_diet - $total_kalori);
		$this->total_kalori = $total_kalori;
	}

	public function getKandunganGiziMakanan()
	{
		$kandunganGizi = new stdClass();
		foreach ($this->_gen as $gen) {
			$kandunganGizi->protein = $gen->_protein;
			$kandunganGizi->lemak = $gen->_lemak;
			$kandunganGizi->karbohidrat = $gen->_karbohidrat;
			$kandunganGizi->sukrosa = $gen->_sukrosa;
			$kandunganGizi->natrium = $gen->_natrium;
			$kandunganGizi->kolesterol = $gen->_kolesterol;
			$kandunganGizi->serat = $gen->_serat;
		}

		return $kandunganGizi;
	}
}
?>