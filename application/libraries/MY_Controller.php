<?php
class MY_Controller extends CI_Controller {

    private $templates = 'templates';

    /*
    * @param : paramater menyimpan request data
    */
    var $request;

    var $data = array(
        'all' => array(),
        'footer' => array(),
        'header' => array(),
        'view' => array()
    );

    function __construct() {
        parent::__construct();
        $this->_initRequest();
        $this->load->helper('date');
        $this->data['header']['title'] = 'Liver Diet App';
    }

    function setTemplate($templates)
    {
        $this->templates = $templates;
    }

    function view($template = null, $isTemplates = true) {
        $output = '';
        $template = implode(
                '/', ($template) ? array_values($template) : array(
                    'controller' => $this->router->class,
                    'method' => $this->router->method
                        )
        );
        $template .= '.php';

        // header template.
        if (file_exists(VIEWPATH . DIRECTORY_SEPARATOR . $this->templates . '/header.php') && $isTemplates) {
            $output .= $this->load->view(
                $this->templates . '/header.php', array_merge($this->data['all'], $this->data['header']), true
            );
            $output .= "\n";
        }
        
        // current template.
        $output .= $this->load->view(
                $template, array_merge($this->data['all'], $this->data['view']), true
        );
        $output .= "\n";

        // footer template.
        if (file_exists(VIEWPATH . DIRECTORY_SEPARATOR . $this->templates .'/footer.php') && $isTemplates) {
            $output .= $this->load->view(
                $this->templates . '/footer.php', array_merge($this->data['all'], $this->data['footer']), true
            );
        }
        
        // trim all tabs and empty lines in order reduce the size of the response.
        $output = preg_replace(
                array('/[\t\s]+(\<)/', '/\n\s*\n/'), array("\n\\1", "\n"), $output
        );

        echo $output;
    }

    function print_v($template = null) {
        $output = '';
        $template = implode(
                '/', ($template) ? array_values($template) : array(
                    'controller' => $this->router->class,
                    'method' => $this->router->method
                        )
        );
        $template .= '.php';

        // current template.
        $output .= $this->load->view(
                $template, array_merge($this->data['all'], $this->data['view']), true
        );

        // trim all tabs and empty lines in order reduce the size of the response.
        $output = preg_replace(
                array('/[\t\s]+(\<)/', '/\n\s*\n/'), array("\n\\1", "\n"), $output
        );

        echo $output;
        exit();
    }

    public function email($template = null) {
        $output = '';
        $template = implode(
                '/', ($template) ? array_values($template) : array(
                    'controller' => $this->router->class,
                    'method' => $this->router->method
                        )
        );
        $template .= '.php';

        // current template.
        $output .= $this->load->view(
                $template, array_merge($this->data['all'], $this->data['view']), true
        );

        // trim all tabs and empty lines in order reduce the size of the response.
        $output = preg_replace(
                array('/[\t\s]+(\<)/', '/\n\s*\n/'), array("\n\\1", "\n"), $output
        );

        return $output;
    }

    function json($data) {
        $this->output->set_header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
        exit();
    }

    function error($data) {
        $this->output->set_status_header('500', 'Internal Server Error');
        $this->output->set_header('Content-type: text/plain');
        echo $data;
        exit();
    }

    function isPost() {
        if (strtoupper($this->input->server('REQUEST_METHOD')) == 'POST') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate absolute redirect uri since the redirect function doesn't
     * handle relative uri that well.
     *
     * @access protected
     *
     * @param string $uri Relative uri path
     * @param array Optional query string parameters
     *
     * @return string $url
     */
    protected function _redirect_uri($uri = '', $params = null) {
        $url = (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
        $url .= '://';
        $url .= $_SERVER['HTTP_HOST'];
        $url .= (array_key_exists('SERVER_PORT', $_SERVER) && (int) $_SERVER['SERVER_PORT'] != 80) ? ':' . $_SERVER['SERVER_PORT'] : '';
        $url .= str_replace(
                basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']
        );
        $url .= trim($uri, '/');
        $url .= ($params) ? '?' . http_build_query($params) : '';

        return $url;
    }

    protected function _initRequest()
    {
        $this->request = new stdClass();
        foreach ($this->input->post() as $key => $value) {
            if(!isset($this->request->{$key}))
                $this->request->{$key} = $value;
        }

        foreach ($this->input->get() as $key => $value) {
            if(!isset($this->request->{$key}))
                $this->request->{$key} = $value;
        }

        $base = base_url();
        if ($this->router->class == "index" && $this->router->method == "index") {
            $base = "";
        }

        $this->data['header']['base'] = $base;
    }
}

?>