<?php 
class MY_Model extends CI_Model
{
	
	private $_table;

	function __construct($table)
	{
		parent::__construct();
		$this->load->database();
		$this->_table = $table;
	}

	public function setTable($table)
	{
		$this->_table = $table;
	}

	public function getFields()
	{
		return $this->db->list_fields($this->_table);
	}

	public function getObject($request)
	{
		$data = array();

		foreach ($this->getFields() as $key => $field) {
			$data[$field] = isset($request[$field])? $request[$field]:'';
		}
		return $data;
	}
}
?>