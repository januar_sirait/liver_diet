<!-- Header Carousel -->
        <header id="home" class="carousel slide">
            <ul class="cb-slideshow">
                <li><span>image1</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>LIVER DIET APP!</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
                <li><span>image2</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>PENJADWALAN MENU MAKANAN UNTUK PASIEN PENYAKIT HATI</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
                <li><span>image3</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>MENGGUNAKAN METODE GENETIC ALGORITHM</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
                <li><span>Image 04</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>S1 - TEKNOLOGI INFORMASI</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
                <li><span>Image 05</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>FAKULTAS ILMU KOMPUTER DAN TEKNOLOGI INFORMASI</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
                <li><span>Image 06</span>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center"><h3>UNIVERSITAS SUMATERA UTARA - 2016</h3></div>
                            </div>
                        </div>
                        <h4>111402048 -- Dian Aria Ningsih</h4>
                    </div>
                </li>
            </ul>
            <div class="intro-scroller">
                <a class="inner-link" href="#bloghome">
                    <div class="mouse-icon" style="opacity: 1;">
                        <div class="wheel"></div>
                    </div>
                </a> 
            </div>          
        </header>
            
        <!-- Page Content -->
        <section id="bloghome">
            <div class="container">
                <div class="text-center">
                <h2>Apa Itu Penyakit Liver?</h2>
                <img class="img-responsive displayed" src="<?php echo(asset_url_images('short.png')) ?>" alt="about">
                </div>
                <div class="row">
                    <div class="col-md-12 homeport1">
                        <div class="col-md-offset-1 col-md-4 col-sm-12 col-xs-12 portfolio-item">
                            <figure class="effect-oscar">
                                <img src="<?php echo(asset_url_images('blog1.jpg')) ?>" alt="img09" class="img-responsive" />
                                <figcaption>
                                    <h2>Apa itu penyakit hati (liver) ?</h2>
                                    <a href="#">View more</a>
                                </figcaption>           
                            </figure>
                            <p class="text-center">Hati (bahasa Yunani: ἡπαρ, hēpar) merupakan kelenjar terbesar di dalam tubuh, terletak dalam rongga perut sebelah kanan, tepatnya di bawah diafragma. Berdasarkan fungsinya, hati juga termasuk sebagai alat ekskresi.</p>
                            <div class="text-center"><a href="https://id.wikipedia.org/wiki/Hati" target="_blank" class="btn btn-primary btn-noborder-radius hvr-bounce-to-bottom">Read More</a></div>
                        </div>
                        <div class="col-md-offset-2 col-md-4 col-sm-12 col-xs-12 portfolio-item">
                            <figure class="effect-oscar">
                                <img src="<?php echo(asset_url_images('blog2.jpg')) ?>" alt="img09" class="img-responsive"/>
                                <figcaption>
                                    <h2>Gejala - gejalanya</h2>
                                    <a href="#">View more</a>
                                </figcaption>           
                            </figure>
                            <p class="text-center">Hati merupakan organ yang menopang kelangsungan hidup hampir seluruh organ lain di dalam tubuh. Oleh karena lokasi yang sangat strategis dan fungsi multi-dimensional, hati menjadi sangat rentan terhadap datangnya berbagai penyakit.</p>
                            <div class="text-center"><a href="https://id.wikipedia.org/wiki/Hati" target="_blank" class="btn btn-primary btn-noborder-radius hvr-bounce-to-bottom">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="text-center">
                            <h2>About Liver Diet App</h2>
                            <img class="img-responsive displayed" src="<?php echo(asset_url_images('short.png')) ?>" alt="Company about"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                    <span class="color-elements">Liver Diet App</span> adalah aplikasi yang dapat membantu pasien penderita penyakit hati dalam menentukan jadwal menu dietnya. Dimana pasien penderita penyaki hati pada umumnya harus mengikuti pola diet tertentu untuk mendukung proses penyembuhannya.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="services">
            <div class="orangeback">
                <div class="container">
                    <div class="text-center homeport2"><h2>Teknologi</h2></div>
                    <div class="row">
                        <div class="col-md-12 homeservices1">
                            <div class="col-md-3 portfolio-item">
                                <div class="text-center">
                                    <a href="javascript:void(0);">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-codepen fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <h3><a href="#">Genetic Algorithm</a></h3>
                                    <p><a href="https://id.wikipedia.org/wiki/Algoritma_genetik" target="_blank" style="font-weight:bold">Algoritma genetika </a> adalah algoritma pencarian (search algorithm) yang menggunakan prinsip seleksi alam dalam ilmu genetika untuk mengembangkan solusi terhadap permasalahan (Haupt dan Haupt, 2004). </p>
                                </div>
                            </div>
                            <div class="col-md-3 portfolio-item">
                                <div class="text-center">
                                    <a href="javascript:void(0);">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-php fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <h3><a href="#">PHP 5.4</a></h3>
                                    <p><a href="https://php.net" target="_blank" style="font-weight:bold">PHP: Hypertext Preprocessor</a> adalah bahasa skrip yang dapat ditanamkan atau disisipkan ke dalam HTML. PHP banyak dipakai untuk memrogram situs web dinamis.</p>
                                </div>
                            </div>

                            <div class="col-md-3 portfolio-item">
                                <div class="text-center">
                                    <a href="javascript:void(0);">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-codeigniter fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <h3><a href="#">CodeIgniter Web Framework</a></h3>
                                    <p><a href="https://www.codeigniter.com/" target="_blank" style="font-weight:bold">CodeIgniter</a> merupakan aplikasi sumber terbuka yang berupa framework PHP dengan model MVC (Model, View, Controller) untuk membangun website dinamis dengan menggunakan PHP. </p>
                                </div>
                            </div>
                            <div class="col-md-3 portfolio-item">
                                <div class="text-center">
                                    <a href="javascript:void(0);">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-bootstrap fa-stack-1x"></i>
                                    </span>
                                    </a>
                                    <h3><a href="#">Bootstrap</a></h3>
                                    <p><a href="http://getbootstrap.com/" target="_blank" style="font-weight:bold">Bootstrap</a> adalah sebuah alat bantu untuk membuat sebuah tampilan halaman website yang dapat mempercepat pekerjaan seorang pengembang website ataupun pendesain halaman website.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio1">
        <div class="container">
        <div class="row">
            <div class="text-center">
            <h2>Developer</h2>
            <img class="img-responsive displayed" src="<?php echo(asset_url_images('short.png')) ?>" alt="about">
            </div>

            <div class="row">
                <div class="col-md-4">
                    <img src="<?php echo(asset_url_images('dian2.jpg')) ?>" class="img-responsive img-circle" alt="Responsive image">
                </div>

                <div class="col-md-6">
                    <h4>Dian Aria Ningsih</h4>
                    <p>
                        Mahasiswa berprestasi tahun 2017. Amiiinnn
                    </p>
                    <p>
                        
                    </p>
                </div>
            </div>
          </div> 
        </div>
        </section>