	<div class="container content">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">
					Schedule Process
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<!-- START Form Wizard -->         
				<form class="form-horizontal form-bordered" action="#" id="wizard">
					<!-- Wizard Container 1 -->
					<div class="wizard-title">Data Pasien</div>
					<div class="wizard-container">
						<div class="form-group">
							<div class="col-md-12">
								<h5 class="semibold text-primary nm">*Data pribadi penderita diabetes melitus.</h5>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-5">
										<input class="form-control" name="nama" id="nama" required></input>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-3">
										<select class="form-control" name="jenis_kelamin" required>
											<option value="">Please choose</option>
											<option value="laki-laki">Laki-laki</option>
											<option value="perempuan">Perempuan</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Usia</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-2">
										<input class="form-control required" name="usia" id="usia"></input>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Berat Badan</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-2">
										<input class="form-control required digits" name="berat_badan" id="berat_badan"></input>
									</div>
									<label class="control-label">kg</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tinggi Badan</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-2">
										<input class="form-control required digits" name="tinggi_badan" id="tinggi_badan"></input>
									</div>
									<label class="control-label">cm</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tingkat Aktivitas</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-3">
										<select class="form-control required" name="tingkat_aktifitas">
											<option value="">Please choose</option>
											<option value="ringan">ringan</option>
											<option value="sedang">sedang</option>
											<option value="berat">berat</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Wizard Container 1 -->

					<!-- Wizard Container 2 -->
					<div class="wizard-title">Informations</div>
					<div class="wizard-container">
						<div class="form-group">
							<div class="col-md-12">
								<h5 class="semibold text-primary nm">Provide some of your details.</h5>
								<p class="text-muted nm">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="bs-callout bs-callout-info">
								<h4>Data Pasien</h4>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama</label>
									<div class="col-sm-4">
										<label class="control-label" id="nama"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Jenis Kelamin</label>
									<div class="col-sm-5">
										<label class="control-label" id="jenis_kelamin"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Usia</label>
									<div class="col-sm-5">
										<label class="control-label" id="usia"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Berat Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="berat_badan"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tinggi Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="tinggi_badan"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tingkat Aktivitas</label>
									<div class="col-sm-5">
										<label class="control-label" id="tingkat_aktifitas"></label>
									</div>
								</div>
							</div>

							<div class="bs-callout bs-callout-info">
								<h4>Berat Badan dan Kebutuhan Kalori</h4>
								<div class="form-group">
									<label class="col-sm-2 control-label">Berat Badan Ideal</label>
									<div class="col-sm-5">
										<label class="control-label" id="bbi">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Kalori Basal</label>
									<div class="col-sm-5">
										<label class="control-label" id="kb">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Koreksi Faktor Aktifitas</label>
									<div class="col-sm-5">
										<label class="control-label" id="kfa">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Koreksi Faktor Usia</label>
									<div class="col-sm-5">
										<label class="control-label" id="kfu">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Index Masa Tubuh</label>
									<div class="col-sm-5">
										<label class="control-label" id="imt">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Total Kebutuhan Kalori</label>
									<div class="col-sm-5">
										<label class="control-label" id="total_kalori">
										</label>
									</div>
								</div>

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Jenis diet</th>
											<th>Energi</th>
											<th>Protein</th>
											<th>Lemak</th>
											<th>Karbohidrat</th>
											<th>Kolesterol</th>
											<th>Serat</th>
											<th>Natrium</th>
											<th>Sukrosa</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<!--/ Wizard Container 2 -->

						<!-- Wizard Container 3 -->
						<div class="wizard-title">Payment</div>
						<div class="wizard-container">
							<div class="form-group">
								<div class="col-md-12">
									<h5 class="semibold text-primary nm">Proceed to payment</h5>
									<p class="text-muted nm">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua.</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Card number</label>
									<div class="col-sm-5">
										<input type="text" name="card-number" class="form-control" data-mask="9999-9999-9999-9999">
									</div>
									<div class="col-sm-5">
										<input type="text" name="security-code" class="form-control" placeholder="Security code">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Name on card</label>
									<div class="col-sm-5">
										<input type="text" name="card-holder" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Expiration</label>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-4">
												<select name="month" class="form-control">
													<option value="">Month</option>
													<option value="1">January</option>
													<option value="2">February</option>
													<option value="3">March</option>
													<option value="4">April</option>
													<option value="5">May</option>
													<option value="6">June</option>
													<option value="7">July</option>
													<option value="8">August</option>
													<option value="9">September</option>
													<option value="10">October</option>
													<option value="11">November</option>
													<option value="12">December</option>
												</select>
											</div>
											<div class="col-sm-4">
												<select name="year" class="form-control">
													<option value="">Year</option>
													<option value="1">2014</option>
													<option value="2">2015</option>
													<option value="3">2016</option>
													<option value="4">2017</option>
													<option value="5">2018</option>
													<option value="6">2019</option>
													<option value="7">2020</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--/ Wizard Container 3 -->
						</form>
						<!--/ END Form Wizard -->
					</div>
				</div>
			</div>