<div class="container content blog">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Menu
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div style="margin-bottom : 10px;">
			<?php $asupan = array("", 
	"Dalam bentuk saring / diblender(Diet Hati 1)", 
	"Dalam bentuk makanan lunak / bubur(Diet Hati 2)", 
	"Dalam bentuk makanan padat (Diet Hati 3)"); ?>
				<span>Nama Pasien : <?php echo($pasien->nama) ?></span>
				<br/>
				<span>Umur : <?php echo($pasien->usia) ?></span>
				<br/>
				<span>Jenis Asupan : <?php echo($asupan[$pasien->jenis_asupan]) ?></span>
			</div>
			<div style="overflow-y: auto;">
				
				<?php
				$day = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
				$index = 0;
				foreach ($menu as $menu_sehari) {
					?>
				<div class="schedule-box col-lg-12 col-xs-12">
					<div class="header"><?php echo($day[$index]) ?></div>
					<div class="body">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th>Jadwal</th>
									<th>Makanan Pokok</th>
									<th>Lauk Pauk</th>
									<th>Sayur</th>
									<th>Buah</th>
									<th>Pelangkap</th>
								</tr>
						<?php
							for ($i = 0; $i < 3; $i++) {
						?>
						<tr>
						<?php
								if ($i == 0) {
									echo('<td>Sarapan</td>');
								}else if($i == 1){
									echo('<td>Makan Siang</td>');
								}else if ($i == 2) {
									echo('<td>Makan Malam</td>');
								}

								echo("<td>". $menu_sehari[$i * 5 + 0]->nama_makanan . " (". $menu_sehari[$i * 5 + 0]->weight ." g)" ."</td>");
								echo("<td>". $menu_sehari[$i * 5 + 1]->nama_makanan . " (". $menu_sehari[$i * 5 + 1]->weight ." g)" ."</td>");
								echo("<td>". $menu_sehari[$i * 5 + 2]->nama_makanan . " (". $menu_sehari[$i * 5 + 2]->weight ." g)" ."</td>");
								echo("<td>". $menu_sehari[$i * 5 + 3]->nama_makanan . " (". $menu_sehari[$i * 5 + 3]->weight ." g)" ."</td>");
								echo("<td>". $menu_sehari[$i * 5 + 4]->nama_makanan . " (". $menu_sehari[$i * 5 + 4]->weight ." g)" ."</td>");
						?>
						</tr>
						<?php
							} 
						?>
							</tbody>
						</table>
					</div>
				</div>

					<?php
					$index++;
				}
				?>
			</div>
		</div>
	</div>
</div>