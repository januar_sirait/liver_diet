<div class="container content blog">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				History
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div style="overflow-y: auto;">
			<table id="table-diet" class="table table-bordered">
				<thead>
					<tr class="active">
						<th>No.</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th>Usia</th>
						<th>Berat Badan</th>
						<th>Tinggi Badan</th>
						<th>Alergi</th>
						<th style="display:none">Makanan yang diinginkan</th>
						<th>Jenis Asupan</th>
						<th>Edema</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1; 
					$asupan = array('', 'Dalam bentuk saring / diblender(Diet Hati 1)', 'Dalam bentuk makanan lunak / bubur(Diet Hati 2)', 'Dalam bentuk makanan padat (Diet Hati 3)');
					foreach ($listPasien as $pasien) {
					?>
					<tr>
						<td><?php echo($i) ?></td>
						<td><?php echo($pasien->nama) ?></td>
						<td><?php echo($pasien->jenis_kelamin) ?></td>
						<td><?php echo($pasien->usia) ?></td>
						<td><?php echo($pasien->berat_badan) ?> kg</td>
						<td><?php echo($pasien->tinggi_badan) ?> cm</td>
						<td><?php echo($pasien->alergi) ?></td>
						<td style="display:none"><?php echo($pasien->makanan_ingin) ?></td>
						<td><?php echo($asupan[$pasien->jenis_asupan]) ?></td>
						<td><?php echo($pasien->edema) ?></td>
						<td>
							<a href="<?php echo(base_url('processing/detail/' . $pasien->id_pasien)) ?>" data-toggle="tooltip" data-placement="top" title="detail">
								<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
							</a>
							<a class="btn-delete" href="#" data-toggle="tooltip" data-placement="top" title="delete" data-id="<?php echo($pasien->id_pasien) ?>">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</a>
						</td>
					</tr>
					<?php
					$i++;
					} ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
     		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Delete</h4>
      		</div>
      		<div class="modal-body">
        		Apakah Anda ingin menghapus data tersebut?
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        		<button type="button" class="btn btn-primary btn-delete-ok" data-id="">Delete</button>
      		</div>
    	</div>
  	</div>
</div>