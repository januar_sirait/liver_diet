<div class="container content blog">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Proses Penjadwalan
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="form-wizard clearfix">
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="first current" aria-disabled="false" aria-selected="true">
						<a id="wizard-t-0" href="#wizard-h-0" aria-controls="wizard-p-0">
							<span class="current-info audible">current step: </span>
							<span class="number">1. Data Pasien</span>
							<!-- <span class="title">Data Pasien</span> -->
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a id="wizard-t-1" href="#wizard-h-1" aria-controls="wizard-p-1">
							<span class="number">2. Data Diet</span>
							<!-- <span class="title">Data Diet</span> -->
						</a>
					</li>
					<li role="tab" class="disabled last" aria-disabled="true">
						<a id="wizard-t-2" href="#wizard-h-2" aria-controls="wizard-p-2">
							<span class="number">3. Jadwal</span>
							<!-- <span class="title">Jadwal</span> -->
						</a>
					</li>
				</ul>

				<div class="content step-body clearfix">

					<form id="form-step-1" class="form-horizontal form-bordered form-step" data-current="true" data-list="#wizard-t-0" data-finished="false">
						<div class="wizard-container">
							<div class="form-group">
								<div class="col-md-12">
									<h5 class="semibold text-primary nm">*Data pribadi penderita penyakit hati.</h5>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="nama" id="nama" required></input>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-3">
											<select class="form-control" name="jenis_kelamin" required>
												<option value="">Please choose</option>
												<option value="laki-laki">Laki-laki</option>
												<option value="perempuan">Perempuan</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Usia</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required" name="usia" id="usia"></input>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Berat Badan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required digits" name="berat_badan" id="berat_badan"></input>
										</div>
										<label class="control-label">kg</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tinggi Badan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required digits" name="tinggi_badan" id="tinggi_badan"></input>
										</div>
										<label class="control-label">cm</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alergi</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="alergi" id="alergi" data-toggle="tooltip" data-placement="top" title="gunakan tanda koma(,) sebagai pemisah jika bahan makanan lebih dari satu item"/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" style="display:none">
								<label class="col-sm-2 control-label">Makan yang diinginkan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="makanan_ingin" id="makanan_ingin" data-toggle="tooltip" data-placement="top" title="gunakan tanda koma(,) sebagai pemisah jika bahan makanan lebih dari satu item"/>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Asupan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-6">
											<select class="form-control required" name="jenis_asupan" data-toggle="tooltip" data-placement="top" title="" data-html="true">
												<option value="">Please choose</option>
												<option value="1">Dalam bentuk saring / diblender(Diet Hati 1)</option>
												<option value="2">Dalam bentuk makanan lunak / bubur(Diet Hati 2)</option>
												<option value="3">Dalam bentuk makanan padat (Diet Hati 3)</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Edema</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-3">
											<select class="form-control required" name="edema" data-toggle="tooltip" data-placement="top" title="" data-html="true">
												<option value="">Please choose</option>
												<option value="tidak ada">tidak ada</option>
												<option value="ringan">ringan</option>
												<option value="sedang berat">sedang berat</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>

					<form id="form-step-2" class="form-horizontal form-bordered form-step form-hide" data-current="true" data-list="#wizard-t-1" data-finished="false">
						<div class="wizard-container">
							<!-- <div class="form-group">
								<div class="col-md-12">
									<h5 class="semibold text-primary nm">Provide some of your details.</h5>
									<p class="text-muted nm">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div> -->
							<div class="bs-callout bs-callout-info">
								<div class="bs-callout-header">
									Data Pasien
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Nama</label>
									<div class="col-sm-4">
										<label class="control-label" id="nama"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Jenis Kelamin</label>
									<div class="col-sm-5">
										<label class="control-label" id="jenis_kelamin"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Usia</label>
									<div class="col-sm-5">
										<label class="control-label" id="usia"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Berat Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="berat_badan"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Tinggi Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="tinggi_badan"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Alergi</label>
									<div class="col-sm-5">
										<label class="control-label" id="alergi"></label>
									</div>
								</div>
								<div class="row" style="display:none">
									<label class="col-sm-2 control-label">Makan yang diinginkan</label>
									<div class="col-sm-5">
										<label class="control-label" id="makanan_ingin"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Jenis Asupan</label>
									<div class="col-sm-5">
										<label class="control-label" id="jenis_asupan"></label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Edema</label>
									<div class="col-sm-5">
										<label class="control-label" id="edema"></label>
									</div>
								</div>
							</div>

							<div class="bs-callout bs-callout-info">
								<div class="bs-callout-header">
									Berat Badan dan Kebutuhan Kalori
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Berat Badan Ideal</label>
									<div class="col-sm-5">
										<label class="control-label" id="bbi">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Edema</label>
									<div class="col-sm-5">
										<label class="control-label" id="berat_edema">
										</label>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label">Total Kebutuhan Kalori</label>
									<div class="col-sm-5">
										<label class="control-label" id="total_kalori">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Jenis Diet</label>
									<div class="col-sm-5">
										<label class="control-label" id="jenis_diet">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Protein</label>
									<div class="col-sm-5">
										<label class="control-label" id="protein">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Lemak</label>
									<div class="col-sm-5">
										<label class="control-label" id="lemak">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Karbohidrat</label>
									<div class="col-sm-5">
										<label class="control-label" id="karbohidrat">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Kalsium</label>
									<div class="col-sm-5">
										<label class="control-label" id="kalsium">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Besi</label>
									<div class="col-sm-5">
										<label class="control-label" id="besi">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Vitamin A</label>
									<div class="col-sm-5">
										<label class="control-label" id="vitamin_a">
										</label>
									</div>
								</div>
								<div class="row">
									<label class="col-sm-2 control-label">Vitamin C</label>
									<div class="col-sm-5">
										<label class="control-label" id="vitamin_c">
										</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Generasi Awal</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<select class="form-control" name="generasi_awal" id="generasi_awal" required>
												<option value="10">10</option>
												<option value="20" selected>20</option>
												<option value="30">30</option>
												<option value="40">40</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Iterasi</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<select class="form-control" name="iterasi" id="iterasi" required>
												<option value="10">10</option>
												<option value="20">20</option>
												<option value="30">30</option>
												<option value="40">40</option>
												<option value="50">50</option>
												<option value="100">100</option>
												<option value="150">150</option>
												<option value="200">200</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-12 loading">
								<img src="<?php echo(asset_url_images('progressbar.gif')) ?>">
							</div>
						</div>
					</form>

					<form id="form-step-3" class="form-horizontal form-bordered form-step form-hide" data-current="false" data-list="#wizard-t-2" data-finished="false">
						<div class="wizard-container">
							<div id="schedule" style="overflow-y: auto;">
								<div class="pull-right">
									<a class="btn btn-primary" href="/processing/show_ga" target="_blank">Show grafik</a>
									<a class="btn btn-primary" href="/processing/show_log/1" target="_blank">GA Log</a>
								</div>
								<label id="result_jenis_diet"></label>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="actions clearfix">
				<ul role="menu" aria-label="Pagination">
					<li class="disabled" aria-disabled="true">
						<a id="previous" href="#previous" role="menuitem" class="btn btn-default">Previous</a>
					</li>
					<li aria-hidden="false" aria-disabled="false">
						<a id="next" href="#next" role="menuitem" class="btn btn-default">Next</a>
					</li>
					<li aria-hidden="true">
						<a id="finish" href="#finish" role="menuitem" class="btn btn-primary" style="display:none">Finish</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<hr/>
</div>