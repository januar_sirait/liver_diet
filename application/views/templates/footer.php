<footer id="footer">
	<div class="container">
		<div class="row myfooter">
			<div class="col-sm-6"><div class="pull-left">
				© Copyright Company 2015 | Dian Aria Ningsih <!-- <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a> -->
			</div></div>
			<div class="col-sm-6">
				<!-- <div class="pull-right">Designed by <a href="http://www.atis.al">ATIS</a></div> -->
			</div>
		</div>
	</div>
</footer>

<!-- jQuery -->
<script src="<?php echo(asset_url_script('jquery.js')) ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo(asset_url_script('bootstrap.min.js')) ?>"></script>

<!-- Portfolio -->
<script src="<?php echo(asset_url_script('jquery.quicksand.js')) ?>"></script>	   

<?php echo(script_tag('steps/js/jquery.steps.min.js', TRUE)) ?> 


<!--Jquery Smooth Scrolling-->
<script>
	$(document).ready(function(){
		$('.custom-menu a[href^="#"], .intro-scroller .inner-link').on('click',function (e) {
			e.preventDefault();

			var target = this.hash;
			var $target = $(target);

			$('html, body').stop().animate({
				'scrollTop': $target.offset().top
			}, 900, 'swing', function () {
				window.location.hash = target;
			});
		});

		$('a.page-scroll').bind('click', function(event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});

		$(".nav a").on("click", function(){
			$(".nav").find(".active").removeClass("active");
			$(this).parent().addClass("active");
		});

		$('body').append('<div id="toTop" class="btn btn-primary color1"><span class="glyphicon glyphicon-chevron-up"></span></div>');
		$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$('#toTop').fadeIn();
			} else {
				$('#toTop').fadeOut();
			}
		}); 
		$('#toTop').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 700);
			return false;
		});

	});

</script>

<script>
	function gallery(){};

	var $itemsHolder = $('ul.port2');
	var $itemsClone = $itemsHolder.clone(); 
	var $filterClass = "";
	$('ul.filter li').click(function(e) {
		e.preventDefault();
		$filterClass = $(this).attr('data-value');
		if($filterClass == 'all'){ var $filters = $itemsClone.find('li'); }
		else { var $filters = $itemsClone.find('li[data-type='+ $filterClass +']'); }
		$itemsHolder.quicksand(
			$filters,
			{ duration: 1000 },
			gallery
			);
	});

	$(document).ready(gallery);
</script>

<?php
	if (isset($scripts)) {
	    foreach ($scripts as $value) {
	        if (is_array($value)) {
	            if(isset($value['assets']) && $value['assets'] == TRUE)
	            {
	                echo(script_tag($value['src'], TRUE));
	            }else{
	                echo(script_tag($value['src']));
	            }
	        }else{
	            echo(script_tag($value));
	        }
	    }
	}
	?>

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>

</body>
</html>