<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo($title) ?></title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo(asset_url_css('bootstrap.min.css')) ?>" type="text/css">

        <!-- Custom Fonts -->
        <link href='<?php echo(asset_url_css('font/OpenSans.css')) ?>' rel='stylesheet' type='text/css'>
        <link href='<?php echo(asset_url_css('font/Merriweather.css')) ?>' rel='stylesheet' type='text/css'>
        <?php echo(link_tag('font-awesome/css/font-awesome.min.css', 'stylesheet', 'text/css', '', '', TRUE)) ?>

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo(asset_url_css('patros.css')) ?>" type="text/css">
        <?php echo(link_tag('steps/css/jquery-steps.min.css', 'stylesheet', 'text/css', '', '', TRUE)) ?>

        <!-- Overwrite style -->
        <link rel="stylesheet" type="text/css" href="<?php echo(asset_url_css('style.css')) ?>">
        <?php
            if (isset($style)) {
                foreach ($style as $value) {
                    if (is_array($value)) {
                        if(isset($value['assets']) && $value['assets'] == true){
                            echo(link_tag($value['href'],'stylesheet', 'text/css','','',TRUE));
                        }else{
                            echo(link_tag($value['href']));
                        }
                    }  else {
                        echo(link_tag($value));
                    }
                }
            }
        ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

<body data-spy="scroll">
    <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        Liver Diet App
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right custom-menu">
                        <li class=""><a href="<?php echo($base)?>#home">Home</a></li>
                        <li><a href="<?php echo($base)?>#bloghome">Apa Itu Penyakit Liver?</a></li>
                        <li><a href="<?php echo($base)?>#about">About</a></li>
                        <li><a href="<?php echo($base)?>#services">Teknologi</a></li>
                        <li><a href="<?php echo($base)?>#portfolio1">Pengembang</a></li>
                        <li class="<?php echo(($this->router->class == 'processing' && $this->router->method == 'index')?'active':'') ?>"><a href="<?php echo(base_url('processing'))?>">Proses Penjadwalan</a></li>
                        <li class="<?php echo(($this->router->class == 'processing' && $this->router->method == 'history')?'active':'') ?>"><a href="<?php echo(base_url('processing/history'))?>">History</a></li>
                    </ul>
                </div>
            </div>
        </nav>
