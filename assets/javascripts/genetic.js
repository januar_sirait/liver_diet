jQuery(document).ready(function($) {
	var asupan = ["", 
	"Dalam bentuk saring / diblender(Diet Hati 1)", 
	"Dalam bentuk makanan lunak / bubur(Diet Hati 2)", 
	"Dalam bentuk makanan padat (Diet Hati 3)"];

	var day = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];

	changeTableList = function () {
		$('.form-step').each(function(index, el) {
			var item = $($(el).attr('data-list')).parent();
			$(item).removeClass('disabled');
			$(item).removeClass('current');
			$(item).removeClass('done');
			if ($(el).attr('data-current') == "true") {
				$(item).addClass('current');
				return;
			}

			if ($(el).attr('data-finished') == "true") {
				$(item).addClass('done');
				return;
			}
			$(item).addClass('disabled');
		});
	}
	$('div.steps').on('click', 'ul > li > a', function(event) {
		event.preventDefault();
		/* Act on the event */
	});

	$('div.actions').on('click', 'ul > li > a#previous', function(event) {
		event.preventDefault();
		if (!$(this).hasClass('disabled')) {
			if ($("#form-step-1").attr('data-current') == "true") {
			}else if ($("#form-step-2").attr('data-current') == "true"){
				$('#form-step-2').addClass('form-hide');
				$('#form-step-1').removeClass('form-hide');
				$("a#previous").parent().addClass('disabled');

				$("#form-step-2").attr('data-current',"false");
				$("#form-step-1").attr('data-current',"true");
				$('#form-step-1').attr('data-finished', 'false');
			}else if ($("#form-step-3").attr('data-current') == "true"){
				$('#form-step-3').addClass('form-hide');
				$('#form-step-2').removeClass('form-hide');

				$("#form-step-3").attr('data-current',"false");
				$("#form-step-2").attr('data-current',"true");
				$('#form-step-2').attr('data-finished', 'false');

				$('ul > li > a#finish').hide();
				$('ul > li > a#next').show();
			};

			changeTableList();
		};
	});

	$('div.actions').on('click', 'ul > li > a#next', function(event) {
		event.preventDefault();
		/* Act on the event */
		if ($("#form-step-1").attr('data-current') == "true") {
			$('#form-step-1').submit();
		}else if ($("#form-step-2").attr('data-current') == "true"){
			$("#form-step-2").submit();
		};
	});

	$('div.actions').on('click', 'ul > li > a#finish', function(event) {
		event.preventDefault();
		/* Act on the event */
		document.location.href = 'processing/save';
	});


	FrmStep1 = function () {
		var data = $("#form-step-1").serialize();
		$('#form-step-1 input').attr('disabled','');
	    $('#form-step-1 select').attr('disabled','');
		$.ajax({    
			url: '/processing/count_weight',
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(response) {
			if(response.status){
				$('#form-step-2 label#nama').html(response.data.user.nama);
				$('#form-step-2 label#jenis_kelamin').html(response.data.user.jenis_kelamin);
				$('#form-step-2 label#usia').html(response.data.user.usia);
				$('#form-step-2 label#berat_badan').html(response.data.user.berat_badan + " kg");
				$('#form-step-2 label#tinggi_badan').html(response.data.user.tinggi_badan + " cm");
				$('#form-step-2 label#alergi').html(response.data.user.alergi);
				$('#form-step-2 label#makanan_ingin').html(response.data.user.makanan_ingin);
				$('#form-step-2 label#jenis_asupan').html(asupan[response.data.user.jenis_asupan]);
				$('#form-step-2 label#edema').html(response.data.user.edema);

				$('#form-step-2 label#bbi').html(response.data.bbi + " kg");
				$('#form-step-2 label#berat_edema').html(response.data.edema);
				$('#form-step-2 label#total_kalori').html(response.data.total_kalori);

				var jenis_diet = response.data.jenis_diet;
				$('#form-step-2 label#jenis_diet').html(jenis_diet.jenis_diet);
				$('#form-step-2 label#protein').html(jenis_diet.protein);
				$('#form-step-2 label#lemak').html(jenis_diet.lemak);
				$('#form-step-2 label#karbohidrat').html(jenis_diet.karbohidrat);
				$('#form-step-2 label#kalsium').html(jenis_diet.kalsium);
				$('#form-step-2 label#besi').html(jenis_diet.besi);
				$('#form-step-2 label#vitamin_a').html(jenis_diet.vitamin_a);
				$('#form-step-2 label#vitamin_c').html(jenis_diet.vitamin_c);

				$('#form-step-1').addClass('form-hide');
				$('#form-step-2').removeClass('form-hide');
				$("a#previous").parent().removeClass('disabled');

				$("#form-step-1").attr('data-current',"false");
				$("#form-step-2").attr('data-current', "true");

				$('#form-step-1').attr('data-finished', 'true');
				changeTableList();
	        }
        })
		.fail(function(err) {
			alert(err);
		})
		.always(function() {
			$('#form-step-1 input').removeAttr('disabled');
			$('#form-step-1 select').removeAttr('disabled');
		});
	}

	FrmStep2 = function(){
		var data = $("#form-step-2").serialize();
		$('#form-step-2 input').attr('disabled','');
	    $('#form-step-2 select').attr('disabled','');
	    $('div.loading img').css('display','table-cell');
		$.ajax({    
			url: '/processing/process',
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(response) {
			if(response.status){
				$('#schedule .schedule-box').remove();

				var diet = response.data;
				var hari = 0;
				$.each(diet, function(index, val) {
					var block = createScheduleBlock(day[hari], val.total_kalori, val.menu)
					$('#schedule').append(block);
					hari++;
				});

				$('label#result_jenis_diet').html("Jenis Asupan : " +asupan[response.user.jenis_asupan]);

				$('#form-step-2').addClass('form-hide');
				$('#form-step-3').removeClass('form-hide');
				$("a#previous").parent().removeClass('disabled');

				$("#form-step-2").attr('data-current',"false");
				$("#form-step-3").attr('data-current', "true");

				$('#form-step-2').attr('data-finished', 'true');
				changeTableList();
				$('ul > li > a#next').hide();
				$('ul > li > a#finish').show();
	        }
        })
		.fail(function(err) {
			alert(err);
		})
		.always(function() {
			$('#form-step-2 input').removeAttr('disabled');
			$('#form-step-2 select').removeAttr('disabled');
			$('div.loading img').css('display','none');
		});
	}

	$('#form-step-1').myvalidate(FrmStep1);
	$('#form-step-2').myvalidate(FrmStep2);


	createScheduleBlock = function(day, total_kalori, data){
		var blockString = '<div class="schedule-box col-lg-12 col-xs-12">'+
								'<div class="header">{day}</div>'+
									'<div class="body">'+
										'<label>Total Kalori : {kalori}</label>'+
										'<table class="table table-bordered">'+
											'<tr>'+
												'<th>Jadwal</th>'+
												'<th>Makanan Pokok</th>'+
												'<th>Lauk Pauk</th>'+
												'<th>Sayur</th>'+
												'<th>Buah</th>'+
												'<th>Pelangkap</th>'+
											'</tr>'+
										'</table>'+
									'</div>'+
								'</div>'+
							'</div>';
		blockString = blockString.replace('{day}', day);
		blockString = blockString.replace('{kalori}', total_kalori);
 
		var block = $(blockString);
		var table = block.find('.table');

		for (var i = 0; i < 3; i++) {
			var row = $('<tr></tr>');
			if (i == 0) {
				row.append('<td>Sarapan</td>');
			}else if(i == 1){
				row.append('<td>Makan Siang</td>');
			}else if (i == 2) {
				row.append('<td>Makan Malam</td>');
			}

			row.append("<td>"+ data[i * 5 + 0].nama_makanan + " ("+ data[i * 5 + 0].weight +" g)" +"</td>");
			row.append("<td>"+ data[i * 5 + 1].nama_makanan + " ("+ data[i * 5 + 1].weight +" g)" +"</td>");
			row.append("<td>"+ data[i * 5 + 2].nama_makanan + " ("+ data[i * 5 + 2].weight +" g)" +"</td>");
			row.append("<td>"+ data[i * 5 + 3].nama_makanan + " ("+ data[i * 5 + 3].weight +" g)" +"</td>");
			row.append("<td>"+ data[i * 5 + 4].nama_makanan + " ("+ data[i * 5 + 4].weight +" g)" +"</td>");

			table.append(row);
		};

		return block;
	}
});