jQuery(document).ready(function($) {
	$('.btn-delete').click(function(event) {
		$('.modal').modal();
		$('.btn-delete-ok').attr('data-id', $(this).attr('data-id'));
	});

	$('.btn-delete-ok').click(function(event) {
		var data_id = $(this).attr('data-id');

		$.ajax({
			url: '/processing/delete/' + data_id,
			type: 'POST',
			dataType: 'json'
		})
		.done(function() {
			document.location.href = '/processing/history';
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			$('.modal').modal('toggle');
		});
		
	});
});