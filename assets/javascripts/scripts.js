//jQuery(document).ready(function($) {
	var form = $("#wizard");
    
	form.steps({
        headerTag: ".wizard-title",
        bodyTag: ".wizard-container",
        onStepChanging: function (event, currentIndex, newIndex) {
        	form.validate().settings.ignore = ":disabled,:hidden";
			
			if(newIndex < currentIndex){
				return true;
			}
			
        	if (form.valid()) {
        		if(currentIndex == 0)
	        	{
	        		$('#wizard-p-0 input').attr('disabled','');
	        		$('#wizard-p-0 select').attr('disabled','');

                    var xmlhttp;
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    try{
                    	xmlhttp.open("POST", "/index/count_weight", false);
						xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
						xmlhttp.send(serialize('#wizard-p-0'));
						if (xmlhttp.readyState == XMLHttpRequest.DONE) {
							var response = $.parseJSON(xmlhttp.responseText);
							if(response.status){
                                $('#wizard-p-0 input').removeAttr('disabled');
                                $('#wizard-p-0 select').removeAttr('disabled');

                                $('label#nama').html(response.data.user.nama);
                                $('label#jenis_kelamin').html(response.data.user.jenis_kelamin);
                                $('label#usia').html(response.data.user.usia);
                                $('label#berat_badan').html(response.data.user.berat_badan);
                                $('label#tinggi_badan').html(response.data.user.tinggi_badan);
                                $('label#tingkat_aktifitas').html(response.data.user.tingkat_aktifitas);

                                $('label#bbi').html(response.data.bbi);
                                $('label#kb').html(response.data.kb);
                                $('label#kfa').html(response.data.kfa);
                                $('label#kfu').html(response.data.kfu);
                                $('label#imt').html(response.data.imt);
                                $('label#total_kalori').html(response.data.total_kalori);
								
								var jenis_diet = response.data.jenis_diet;
								var row =$("<tr></tr>");
								row.append("<td>"+ jenis_diet.jenis_diet +"</td>");
								row.append("<td>"+ jenis_diet.energi +"</td>");
								row.append("<td>"+ jenis_diet.protein +"</td>");
								row.append("<td>"+ jenis_diet.lemak +"</td>");
								row.append("<td>"+ jenis_diet.karbohidrat +"</td>");
								row.append("<td>"+ jenis_diet.kolesterol +"</td>");
								row.append("<td>"+ jenis_diet.serat +"</td>");
								row.append("<td>"+ jenis_diet.natrium +"</td>");
								row.append("<td>"+ jenis_diet.sukrosa +"</td>");
								$('table').append(row);
								return true;
							}
						}else if(xmlhttp.status == 400) {
							alert('There was an error 400');
						}else {
							alert('something else other than 200 was returned');
						}
                    }catch(err)
                    {
                    	alert(xmlhttp.status + " - " + xmlhttp.statusText);
                    }


                    // Using async ajax
					/*
                    $.ajax({    
                        url: '/index/count_weight',
                        type: 'POST',
                        dataType: 'json',
                        data: serialize('#wizard-p-0'),
                    })
                    .done(function(response) {
                        if(response.status){
                            $('#wizard-p-0 input').removeAttr('disabled');
                            $('#wizard-p-0 select').removeAttr('disabled');

                            $('label#nama').html(response.data.user.nama);
                            $('label#jenis_kelamin').html(response.data.user.jenis_kelamin);
                            $('label#usia').html(response.data.user.usia);
                            $('label#berat_badan').html(response.data.user.berat_badan);
                            $('label#tinggi_badan').html(response.data.user.tinggi_badan);
                            $('label#tingkat_aktifitas').html(response.data.user.tingkat_aktifitas);

                            $('label#bbi').html(response.data.bbi);
                            $('label#kb').html(response.data.kb);
                            $('label#kfa').html(response.data.kfa);
                            $('label#kfu').html(response.data.kfu);
                            $('label#imt').html(response.data.imt);
                            $('label#total_kalori').html(response.data.total_kalori);

                            var jenis_diet = response.data.jenis_diet;
                            var row =$("<tr></tr>");
                            row.append("<td>"+ jenis_diet.jenis_diet +"</td>");
                            row.append("<td>"+ jenis_diet.energi +"</td>");
                            row.append("<td>"+ jenis_diet.protein +"</td>");
                            row.append("<td>"+ jenis_diet.lemak +"</td>");
                            row.append("<td>"+ jenis_diet.karbohidrat +"</td>");
                            row.append("<td>"+ jenis_diet.kolesterol +"</td>");
                            row.append("<td>"+ jenis_diet.serat +"</td>");
                            row.append("<td>"+ jenis_diet.natrium +"</td>");
                            row.append("<td>"+ jenis_diet.sukrosa +"</td>");
                            $('table').append(row);
                            $(this).next();
                            //return true;
                        }
                    })
                    .fail(function(err) {
                        alert(err);
                    })
                    .always(function() {
                        $('#wizard-p-0 input').removeAttr('disabled');
                        $('#wizard-p-0 select').removeAttr('disabled');
                    });
                    */
	        	}
        	}

        	//return false;
        },
        onStepChanged: function (event, currentIndex, priorIndex){
        	// Used to skip the "Warning" step if the user is old enough.
            console.log(currentIndex);
        },
        onFinished: function () {
            // do anything here ;)
            alert("finished!");
        }
    }).validate({
    	errorPlacement: function errorPlacement(error, element) { 
    		var parent = element.parent().parent().parent().parent();
    		parent.addClass('has-error');
    		element.parent().parent().after(error.addClass('control-label')); 
    	},
    	success: function(label) {
            var placement = $(label).parent().parent();
            placement.removeClass('has-error');
            label.remove();
        },
    	rules: {
    		confirm: {
    			equalTo: "#password-2"
    		}
    	}
    });
    

    preprocessCallback = function(obj){
    	return true;
    }

    preprocessErrorCallback = function(){
    	return false;
    }

    window.serialize = function(selector) {
    	var data = "";

    	//serialize input
    	$(selector).find('input:text, input:hidden, input:radio:checked, input:checkbox:checked').each(function(index, el) {
    		data += $(el).attr('name') + "=" + $(el).val() + "&";
    	});

    	//serialize select
    	$(selector).find('select').each(function(index, el) {
    		data += $(el).attr('name') + "=" + $(el).val() + "&";
    	});

    	//serialize textarea
    	$(selector).find('textarea').each(function(index, el) {
    		data += $(el).attr('name') + "=" + $(el).val() + "&";
    	});

    	return data;
    }
//});