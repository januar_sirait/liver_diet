jQuery(document).ready(function($) {
	var options = {
		title: 'Perubahan Fitness Terhadap Iterasi (Populasi awal {populasi})',
        captureRightClick: true,
        animate: false,
        animateReplot: false,
        seriesColors : ['#9C27B0'],
        seriesDefaults: {
            // renderer: $.jqplot.BarRenderer,
            pointLabels: { show: true },
            label : 'Fitness'
        },
        axes: {
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    angle: -15,
                    fontSize: '9pt'
                },
                label: 'Iterasi',
                labelOptions: {
                    fontFamily: 'Helvetica',
                    fontSize: '14pt'
                }
            },
            yaxis: {
                label: 'Fitness',
                labelOptions: {
                    fontFamily: 'Helvetica',
                    fontSize: '14pt'
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                min: 0
            }
        },
        legend : {
	        show: true,
	        location: 'ne',
	        placement: 'insideGrid'
	    }
    };

    var data = [0,0,0,0];

    // var ticks = ['May', 'June', 'July', 'August'];
    // options.axes.xaxis.ticks = ticks;

    var plot1 = $.jqplot('placeholder', [data], options);


	$.ajax({
		url: '/processing/show_ga',
		type: 'post',
		dataType: 'json',
	})
	.done(function(response) {
		if (plot1) {
			plot1.destroy();
		}

	    options.animate = true;
	    options.title = options.title.replace("{populasi}", response.populasi)

        if (response.data.length >=50) {
            data = [];
            var step = parseInt((response.data.length / 10) / 2);
            var i = 1;
            while(i <= response.data.length)
            {
                data.push([response.data[i][0], response.data[i][1]]);
                i+= 2;
            }
            plot1 = $.jqplot('placeholder', [data], options);
        }else{
            plot1 = $.jqplot('placeholder', [response.data], options);
        };
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

});