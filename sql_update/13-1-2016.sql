ALTER TABLE `pasien`   
  ADD COLUMN `makanan_ingin` VARCHAR(250) NULL AFTER `tingkat_aktifitas`,
  ADD COLUMN `alergi` VARCHAR(250) NULL AFTER `makanan_ingin`;

ALTER TABLE `menu`  
  ADD FOREIGN KEY (`id_pasien`) REFERENCES `pasien`(`id_pasien`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `lever_diet`.`pasien`   
  ADD COLUMN `jenis_asupan` INT(1) NULL AFTER `alergi`,
  ADD COLUMN `endema` VARCHAR(20) NULL AFTER `jenis_asupan`;
